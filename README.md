# xtractx

Extract Text From Images and save as txt,docx format

![](assets/img/xtractx.png?raw=true)

## Run Locally:

```
$ git clone https://github.com/decentblocks/xtractx.git
$ cd xtractx
$ npm install
$ npm run start
```
### Steps Required :

1. Create a new file .env and copy and replace the contents of env.example with credentials.

2. Save the key.json file inside the project folder after downloading from google api credentials.

This Project uses [Google Cloud Vision](https://cloud.google.com/vision/) api.

You might need to enable billing and the cloud vision api using your google cloud services account.


