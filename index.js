var express = require('express');
var fs = require('fs');
var util = require('util');
var mime = require('mime');
var multer = require('multer');
var path = require('path');
var morgan = require('morgan');
var dotenv = require('dotenv');

//create an instance of express
const app = express();

// //handle file uploads

app.use('/assets', express.static('assets'));

var fileStorage = multer.diskStorage({
	destination: function(req, file, callback) {
		callback(null, 'converted/')
	},
	filename: function(req, file, callback) {
		console.log(file)
		callback(null, file.originalname )
	}
});

var upload = multer({
  storage: fileStorage
});

//use environment variables
dotenv.config()

// Imports the Google Cloud client library.
const Storage = require('@google-cloud/storage');

//use morgan 
app.use(morgan('combined'));

// Instantiates a client.
const storage = new Storage({
  keyFilename: 'key.json'
});

// Makes an authenticated API request.
storage
  .getBuckets()
  .then((results) => {
    const buckets = results[0];

   // console.log('Buckets:');
    buckets.forEach((bucket) => {
      console.log(bucket.name);
    });
  })
  .catch((err) => {
    console.error('ERROR:', err);
  });


//serve index.html
app.get('/', function(req, res){
   res.sendFile(path.join(__dirname + '/views/index.html'));
});


// vision text detection function
function detectText(fileName, res){
    console.log(fileName);
    //Import the Google Cloud Vision library
    const vision = require('@google-cloud/vision');
    
    //creates a client 
    const client = new vision.ImageAnnotatorClient();
    
    //Performs text detection on the uploaded file
    client
      .textDetection(fileName)
      .then(results => {
          const detections = results[0].textAnnotations;
     //     console.log('Text:');
          const Text = JSON.parse(JSON.stringify(detections[0].description, null, 4));
            // console.log(Text);
            res.writeHead(200, {
                 'Content-Type': 'text/html'
             });
             res.write('<!DOCTYPE HTML><html><head><title> xtractx </title><style>.container{float:left;width:440px;border:1px dotted #000;margin:20px;}</style></head><body>');
                
           // Base64 the image so we can display it on the page
            res.write('<div class="container"><img width=400 src="' + base64Image(fileName) + '"><br></div>');
                
            // Write out the extracted text
            res.write('<div class="container">'+ Text + '</div>');
              
            savetowordFile(fileName, Text);

            // Delete image file 
            fs.unlinkSync(fileName);

            res.end('</body></html>');
      })
      .catch(err => {
          console.error('CLOUD VISION ERROR:', err);
      });
     
    }

    // Turn image into Base64 so we can display it easily

    function base64Image(src) {
      var data = fs.readFileSync(src).toString('base64');
      return util.format('data:%s;base64,%s', mime.getType(src), data);
    }
    

    // To save the text in word(.docx) file 
    function savetowordFile(filename, filecontent){
          //save the extracted text in .docx format
          fs.writeFile(filename + '.txt', filecontent , function (err) {
            if (err) throw err;

            console.log('xtraction successful in word format !');
          });
    }
     
    // To save the text in text (.txt) file
    function savetotextFile(filename, filecontent){
           //save the extracted text in .txt format
           fs.writeFile(filename + '.txt', filecontent , function (err) {
            if (err) throw err;

            console.log('xtraction successful in text format !');
          });
    }

//handle image upload , Image is uploaded to req.file.path
app.post('/upload', upload.single('image'), function(req,res,next){
  
   //detect text from the uploaded image
    detectText(req.file.path, res);
    
  });

//Serve at port 8080 
app.listen(8080);
console.log('Server Started at http://localhost:8080');
